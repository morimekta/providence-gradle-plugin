/*
 * Copyright (c) 2016, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.providence.gradle

import net.morimekta.providence.generator.GeneratorOptions
import net.morimekta.providence.generator.format.java.JavaGenerator
import net.morimekta.providence.generator.util.FileManager
import net.morimekta.providence.reflect.ProgramLoader
import org.gradle.api.DefaultTask
import org.gradle.api.Task
import org.gradle.api.tasks.SourceSet
import org.gradle.api.tasks.SourceSetContainer

import java.nio.file.Paths

import static net.morimekta.providence.reflect.util.ReflectionUtils.isThriftBasedFileSyntax

/**
 * Plugin definition for the providence gradle plugin.
 */
abstract class BaseGenerateProvidenceTask extends DefaultTask {
    protected init(boolean _testing) {
        ProvidenceExtension extension = (ProvidenceExtension) project.extensions.getByName('providence')

        def defaultInput = project.fileTree(_testing ? 'src/test/providence' : 'src/main/providence') {
            include '**/*.thrift'
        }

        if (_testing) {
            inputs.files(defaultInput)
            extension.testTask = this
            extension.test = extension.test == null ? new ProvidenceExtensionParams(defaultInput) : extension.test
        } else {
            inputs.files(defaultInput)
            extension.mainTask = this
            extension.main = extension.main == null ? new ProvidenceExtensionParams(defaultInput) : extension.main
        }

        if (_testing) {
            outputs.dir("${project.buildDir}/generated-test-sources/providence")
        } else {
            outputs.dir("${project.buildDir}/generated-sources/providence")
        }

        SourceSetContainer sourceSets = (SourceSetContainer) project.properties.get('sourceSets')
        SourceSet sourceSet = sourceSets.getByName(_testing ? 'test' : 'main')
        sourceSet.allJava.srcDir(outputs.files.asPath)
        sourceSet.java.srcDir(outputs.files.asPath)

        def taskDependsOnThis = _testing ? 'compileTestJava' : 'compileJava'

        Task compile = project.tasks.getByName(taskDependsOnThis)
        if (!compile) {
            throw new NullPointerException('Error: providence plugin must be applied *after* the java plugin')
        }
        compile.dependsOn(this)

        def self = this
        doLast({
            ProvidenceExtensionParams params = _testing ? extension.test : extension.main
            def includes = params.include == null ? [] : params.include.files
            if (params.input.files.empty) {
                return
            }
            GeneratorOptions options = new GeneratorOptions()
            options.generator_program_name = 'providence-gradle-plugin'
            options.program_version = providenceVersion()

            def manager = new FileManager(Paths.get(self.outputs.files.asPath))
            def loader = new ProgramLoader(params.requireFieldId, params.requireEnumValue, params.allowLanguageReservedNames)

            params.input.files.each { file ->
                logger.info("Input file: {}", file.toPath())
                if (isThriftBasedFileSyntax(file.toPath())) {
                    def generator = new JavaGenerator(
                            manager,
                            options,
                            params)
                    generator.generate(loader.load(file))
                }
            }
        })
    }

    static String providenceVersion() {
        Properties applicationProperties = new Properties()
        applicationProperties.load((InputStream) BaseGenerateProvidenceTask.class.getResourceAsStream("/application.properties"))
        return applicationProperties.getProperty("providence.version")
    }
}
